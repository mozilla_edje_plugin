#include "plugin.h"

/* 
 * Implementations of plugin scriptability API functions
 */

#define DEBUG(_id) printf ("debug: %s\n", _id)

static NPObject *
mep_edje_allocate (NPP npp, NPClass *class)
{
	DEBUG ("mep_edje_allocate");
	mep_edje_NPObject *mep_npobj = (mep_edje_NPObject *) NPN_MemAlloc (sizeof (mep_edje_NPObject));
	mep_npobj->_class = class;
	mep_npobj->referenceCount = 1;
	mep_npobj->instance = npp;

	return (NPObject *) mep_npobj;
}

static void
mep_edje_deallocate (NPObject *npobj)
{
	DEBUG ("mep_deallocate");
	mep_edje_NPObject *mep_npobj = (mep_edje_NPObject *) npobj;
	NPN_MemFree (mep_npobj);
}

static void
mep_edje_invalidate (NPObject *npobj)
{
	DEBUG ("mep_invalidate");
	// TODO
}

static char *mep_edje_methods [] = {
	"signal_emit",
	"signal_callback_add",
	NULL // sentinel
};

static bool
mep_edje_hasMethod (NPObject *npobj, NPIdentifier name)
{
	DEBUG ("mep_hasMethod");
	char  *mep_name = (char *) NPN_UTF8FromIdentifier (name);
	int i = 0;
	while (mep_edje_methods[i])
	{
		if (!strcmp (mep_edje_methods[i], mep_name))
			return true;
		i += 1;
	}
	return false;
}

void
mep_edje_signal_callback (void *data, Evas_Object *edj, const char *emission, const char *source)
{
	DEBUG ("mep_signal_callback");
	printf ("edje signal callback: %s %s %s\n", (const char *) data, emission, source);

	PluginInstance *pi = (PluginInstance *) evas_object_data_get (edj, "pi");
	uint32_t num_args = 2;
	NPVariant args [2];
	args[1].type = NPVariantType_String;
	args[1].value.stringValue.utf8characters = emission;
	args[2].type = NPVariantType_String;
	args[2].value.stringValue.utf8characters = source;
	NPVariant res;

	NPObject *window;
	NPN_GetValue (pi->instance, NPNVWindowNPObject, &window);
	NPN_Invoke (
		pi->instance,
		window,
		NPN_GetStringIdentifier ((const char *) data),
		(const NPVariant *) &args,
		num_args,
		&res
	);
	NPN_ReleaseVariantValue (&res);
}

static bool
mep_edje_invoke (NPObject *npobj, NPIdentifier name, const NPVariant *args, uint32_t argCount, NPVariant *result)
{
	DEBUG ("mep_invoke");
	mep_edje_NPObject *mep_npobj = (mep_edje_NPObject *) npobj;
	PluginInstance *pi = (PluginInstance *) (mep_npobj->instance->pdata);
	char  *mep_name = (char *) NPN_UTF8FromIdentifier (name);
	printf ("mep_invoke name: %s\n", mep_name);

	if (!strcmp (mep_name, "signal_emit"))
	{
		DEBUG ("mep_invoke: signal_emit");
		edje_object_signal_emit (
			pi->edj,
			args[0].value.stringValue.utf8characters, // signal
			args[1].value.stringValue.utf8characters); // sender
		return true;
	}
	else if (!strcmp (mep_name, "signal_callback_add"))
	{
		DEBUG ("mep_invoke: signal_callback_add");
		const char *func = strdup (args[2].value.stringValue.utf8characters);
		// FIXME free string somewhere
		edje_object_signal_callback_add (
			pi->edj,
			args[0].value.stringValue.utf8characters, // signal
			args[1].value.stringValue.utf8characters, // sender
			mep_edje_signal_callback,
			(void *) func
		);
		printf ("function: %s\n", args[2].value.stringValue.utf8characters);
		return true;
	}
	else
		return false;
	// TODO
}

static bool
mep_edje_invokeDefault (NPObject *npobj, const NPVariant *args, uint32_t agrCount, NPVariant *result)
{
	DEBUG ("mep_invokeDefault");
	// TODO
}

static char *mep_edje_properties [] = {
	"frametime",
	"play",
	"animation",
	"size_min",
	"size_max",
	NULL // sentinel
};

static bool
mep_edje_hasProperty (NPObject *npobj, NPIdentifier name)
{
	DEBUG ("mep_hasProperty");
	mep_edje_NPObject *mep_npobj = (mep_edje_NPObject *) npobj;
	PluginInstance *pi = (PluginInstance *) (mep_npobj->instance->pdata);

	// check for property
	char  *mep_name = (char *) NPN_UTF8FromIdentifier (name);
	int i = 0;
	while (mep_edje_properties[i])
	{
		if (!strcmp (mep_edje_properties[i], mep_name))
			return true;
		i += 1;
	}

	// check for edje part
	return (bool) edje_object_part_exists (pi->edj, mep_name);
}

static bool
mep_edje_getProperty (NPObject *npobj, NPIdentifier name, NPVariant *result)
{
	// TODO differentiate between Edje and Edje_Part object
	DEBUG ("mep_getProperty");
	mep_edje_NPObject *mep_npobj = (mep_edje_NPObject *) npobj;
	PluginInstance *pi = (PluginInstance *) (mep_npobj->instance->pdata);
	char  *mep_name = (char *) NPN_UTF8FromIdentifier (name);
	printf ("mep_getProperty name: %s\n", mep_name);

	if (!strcmp (mep_name, "frametime"))
	{
		result->type = NPVariantType_Bool;
		result->value.boolValue = edje_object_frametime_get (pi->edj);
		return true;
	}
	// TODO add remaining properties
	else if (edje_object_part_exists (pi->edj, mep_name))
	{
		result->type = NPVariantType_Object;
		result->value.objectValue = NPN_CreateObject (
			mep_npobj->instance,
			&mep_part_class);
		mep_part_NPObject *part = (mep_part_NPObject *) result->value.objectValue;
		part->part = strdup (mep_name);
		// FIXME free me
		return true;
	}
	else
		return false;
}

static bool
mep_edje_setProperty (NPObject *npobj, NPIdentifier name, const NPVariant *value)
{
	DEBUG ("mep_setProperty");
	mep_edje_NPObject *mep_npobj = (mep_edje_NPObject *) npobj;
	PluginInstance *pi = (PluginInstance *) (mep_npobj->instance->pdata);
	char  *mep_name = (char *) NPN_UTF8FromIdentifier (name);
	printf ("mep_getProperty name: %s\n", mep_name);

	if (!strcmp (mep_name, "frametime"))
	{
		edje_object_frametime_set (
			pi->edj,
			value[0].value.boolValue);
		return true;
	}
	// TODO add remaining properties
	else
		return false;
}

static bool
mep_edje_removeProperty (NPObject *npobj, NPIdentifier name)
{
	DEBUG ("mep_removeProperty");
	// TODO
}

static bool
mep_edje_enumerate (NPObject *npobj, NPIdentifier **value, uint32_t *count)
{
	DEBUG ("mep_enumerate");
	// TODO
}

static bool
mep_edje_construct (NPObject *npobj, const NPVariant *args, uint32_t argCount, NPVariant *result)
{
	DEBUG ("mep_construct");
	// TODO
}

struct NPClass mep_edje_class = {
	NP_CLASS_STRUCT_VERSION,
	mep_edje_allocate,
	mep_edje_deallocate,
	mep_edje_invalidate,
	mep_edje_hasMethod,
	mep_edje_invoke,
	mep_edje_invokeDefault,
	mep_edje_hasProperty,
	mep_edje_getProperty,
	mep_edje_setProperty,
	mep_edje_removeProperty,
	mep_edje_enumerate,
	mep_edje_construct
};

/*
 * class functions for object edje_part
 */

static NPObject *
mep_part_allocate (NPP npp, NPClass *class)
{
	DEBUG ("mep_part_allocate");
	mep_part_NPObject *mep_npobj = (mep_part_NPObject *) NPN_MemAlloc (sizeof (mep_part_NPObject));
	mep_npobj->_class = class;
	mep_npobj->referenceCount = 1;
	mep_npobj->instance = npp;
	mep_npobj->part = NULL;

	return (NPObject *) mep_npobj;
}

static void
mep_part_deallocate (NPObject *npobj)
{
	DEBUG ("mep_deallocate");
	mep_part_NPObject *mep_npobj = (mep_part_NPObject *) npobj;
	NPN_MemFree (mep_npobj->part);
	NPN_MemFree (mep_npobj);
}

static void
mep_part_invalidate (NPObject *npobj)
{
	DEBUG ("mep_invalidate");
	// TODO
}

static char *mep_part_methods [] = {
	"swallow",
	"unswallow",
	NULL // sentinel
};

static bool
mep_part_hasMethod (NPObject *npobj, NPIdentifier name)
{
	DEBUG ("mep_hasMethod");
	char  *mep_name = (char *) NPN_UTF8FromIdentifier (name);
	int i = 0;
	while (mep_part_methods[i])
	{
		if (!strcmp (mep_part_methods[i], mep_name))
			return true;
		i += 1;
	}
	return false;
}

static bool
mep_part_invoke (NPObject *npobj, NPIdentifier name, const NPVariant *args, uint32_t argCount, NPVariant *result)
{
	DEBUG ("mep_part_invoke");
	mep_part_NPObject *mep_npobj = (mep_part_NPObject *) npobj;
	PluginInstance *pi = (PluginInstance *) (mep_npobj->instance->pdata);
	char  *mep_name = (char *) NPN_UTF8FromIdentifier (name);
	printf ("mep_part_invoke name: %s\n", mep_name);

	if (!strcmp (mep_name, "swallow"))
	{
		// TODO
		return true;
	}
	if (!strcmp (mep_name, "unswallow"))
	{
		// TODO
		return true;
	}
	else
		return false;
	// TODO
}

static bool
mep_part_invokeDefault (NPObject *npobj, const NPVariant *args, uint32_t agrCount, NPVariant *result)
{
	DEBUG ("mep_invokeDefault");
	// TODO
}

static char *mep_part_properties [] = {
	"geometry",
	"text",
	"drag_dir",
	"drag_value",
	"drag_size",
	"drag_step",
	"drag_page",
	NULL // sentinel
};

static bool
mep_part_hasProperty (NPObject *npobj, NPIdentifier name)
{
	DEBUG ("mep_hasProperty");
	mep_part_NPObject *mep_npobj = (mep_part_NPObject *) npobj;
	PluginInstance *pi = (PluginInstance *) (mep_npobj->instance->pdata);

	// check for property
	char  *mep_name = (char *) NPN_UTF8FromIdentifier (name);
	int i = 0;
	while (mep_part_properties[i])
	{
		if (!strcmp (mep_part_properties[i], mep_name))
			return true;
		i += 1;
	}

	// check for edje part
	return (bool) edje_object_part_exists (pi->edj, (char *) name);
}

static bool
mep_part_getProperty (NPObject *npobj, NPIdentifier name, NPVariant *result)
{
	// TODO differentiate between Edje and Edje_Part object
	DEBUG ("mep_getProperty");
	mep_part_NPObject *mep_npobj = (mep_part_NPObject *) npobj;
	PluginInstance *pi = (PluginInstance *) (mep_npobj->instance->pdata);
	char  *mep_name = (char *) NPN_UTF8FromIdentifier (name);
	printf ("mep_getProperty name: %s\n", mep_name);

	if (!strcmp (mep_name, "text"))
	{
		result->type = NPVariantType_String;
		result->value.stringValue.utf8characters = edje_object_part_text_get (pi->edj, mep_npobj->part);
		return true;
	}
	// TODO add remaining properties
	else
		return false;
}

static bool
mep_part_setProperty (NPObject *npobj, NPIdentifier name, const NPVariant *value)
{
	DEBUG ("mep_setProperty");
	mep_part_NPObject *mep_npobj = (mep_part_NPObject *) npobj;
	PluginInstance *pi = (PluginInstance *) (mep_npobj->instance->pdata);
	char  *mep_name = (char *) NPN_UTF8FromIdentifier (name);

	if (!strcmp (mep_name, "text"))
	{
		edje_object_part_text_set (
			pi->edj,
			mep_npobj->part,
			value[0].value.stringValue.utf8characters
		);
		return true;
	}
	// TODO add remaining properties
	else
		return false;
}

static bool
mep_part_removeProperty (NPObject *npobj, NPIdentifier name)
{
	DEBUG ("mep_removeProperty");
	// TODO
}

static bool
mep_part_enumerate (NPObject *npobj, NPIdentifier **value, uint32_t *count)
{
	DEBUG ("mep_enumerate");
	// TODO
}

static bool
mep_part_construct (NPObject *npobj, const NPVariant *args, uint32_t argCount, NPVariant *result)
{
	DEBUG ("mep_construct");
	// TODO
}

struct NPClass mep_part_class = {
	NP_CLASS_STRUCT_VERSION,
	mep_part_allocate,
	mep_part_deallocate,
	mep_part_invalidate,
	mep_part_hasMethod,
	mep_part_invoke,
	mep_part_invokeDefault,
	mep_part_hasProperty,
	mep_part_getProperty,
	mep_part_setProperty,
	mep_part_removeProperty,
	mep_part_enumerate,
	mep_part_construct
};

