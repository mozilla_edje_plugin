#include "plugin.h"

/*
 * Implementations of plugin API functions
 */

void
mep_asynccall (void *data)
{
	PluginInstance *pi = (PluginInstance *) data;
	ecore_main_loop_iterate ();
	if (!pi->done)
	{
		usleep (1000); // max 100 fps
		NPN_PluginThreadAsyncCall (pi->instance, mep_asynccall, (void *) pi);
	}
}

int
mep_animator (void *data)
{
	PluginInstance *pi = (PluginInstance *) data;
	evas_render (pi->evas);
	/* needed for windowless mode
	NPRect rect;
	rect.top = 0;
	rect.left = 0;
	rect.bottom = 400;
	rect.right = 400;
	NPN_InvalidateRect (pi->instance, &rect);

	NPN_ForceRedraw (pi->instance);
	*/
	return 1;
}


void
mep_event_handler (Widget xtwidget, void *data, XEvent *xevent, Boolean *b)
{
	PluginInstance *pi = (PluginInstance *) data;
	Evas *evas = pi->evas;
	XEvent ev = *xevent;

	//printf ("event type: %d\n", ev.type);

	if (ev.type > 0)
  switch (ev.type) {
	  case ButtonPress:
			evas_event_feed_mouse_move(evas, ev.xbutton.x, ev.xbutton.y, 0, NULL);
		  evas_event_feed_mouse_down(evas, ev.xbutton.button, EVAS_BUTTON_NONE, 0, NULL);
		  break;
	  case ButtonRelease:
		  evas_event_feed_mouse_move(evas, ev.xbutton.x, ev.xbutton.y, 0, NULL);
		  evas_event_feed_mouse_up(evas, ev.xbutton.button, EVAS_BUTTON_NONE, 0, NULL);
			break;
	  case MotionNotify:
		  evas_event_feed_mouse_move(evas, ev.xmotion.x, ev.xmotion.y, 0, NULL);
		break;
	  case Expose:
		  evas_damage_rectangle_add(evas,
					    ev.xexpose.x,
					    ev.xexpose.y,
					    ev.xexpose.width,
					    ev.xexpose.height);
		  break;
	  case ConfigureNotify:
		  evas_output_size_set(evas,
				       ev.xconfigure.width,
				       ev.xconfigure.height);
		  break;
		case EnterNotify:
			evas_object_focus_set (pi->edj, 1);
			//trigger (pi);
			break;
		case LeaveNotify:
			evas_object_focus_set (pi->edj, 0);
			//trigger (pi);
			break;
		case ClientMessage: // 33
			if (ev.xclient.message_type == pi->atom)
				ecore_main_loop_iterate ();
			break;
	  default:
		  break;
  }
}

NPObject *
mep_getscriptableobject (PluginInstance *pi)
{
	if (!pi->scriptable_object)
	{
    pi->scriptable_object = NPN_CreateObject (
			pi->instance,
			&mep_edje_class);
  }

  if (pi->scriptable_object)
	{
    NPN_RetainObject (pi->scriptable_object);
  }

  return pi->scriptable_object;
}

void
mep_cb_ui_play (void *data, Evas_Object *edj, const char *emission, const char *source)
{
	PluginInstance *pi = (PluginInstance *) data;
	if (!edje_object_play_get (pi->edj)) // if paused
	{
		edje_object_play_set (pi->edj, 1);
		//edje_object_animation_set (pi->edj, 1);
	}
}

void
mep_cb_ui_pause (void *data, Evas_Object *edj, const char *emission, const char *source)
{
	PluginInstance *pi = (PluginInstance *) data;
	if (edje_object_play_get (pi->edj)) // if playing
	{
		edje_object_play_set (pi->edj, 0);
		//edje_object_animation_set (pi->edj, 0);
	}
}

void
mep_cb_ui_save (void *data, Evas_Object *edj, const char *emission, const char *source)
{
	PluginInstance *pi = (PluginInstance *) data;
	NPN_GetURL (pi->instance, pi->data, "_parent");
}

void
mep_cb_ui_config (void *data, Evas_Object *edj, const char *emission, const char *source)
{
	PluginInstance *pi = (PluginInstance *) data;
	// TODO
}

void
mep_cb_all (void *data, Evas_Object *edj, const char *emission, const char *source)
{
	printf ("edje callback: %s %s\n", emission, source);
}

void
mep_configure (PluginInstance *pi)
{
	char config_path[256];
	sprintf (config_path, "%s/%s", CONFIGDIR, "config.edb");
	ecore_config_file_load (config_path);
	pi->config_theme = strdup (ecore_config_theme_get ("theme"));
	pi->config_engine = strdup (ecore_config_string_get ("engine"));
	pi->config_fps = ecore_config_int_get ("fps");
}

