#include "plugin.h"

/*
 * Implementations of plugin API functions
 */

char*
NPP_GetMIMEDescription (void)
{
	return (MIME_TYPES_HANDLED);
}

/* TODO: set this variables at the right place to enable scriptability
 * NPPVpluginScriptableInstance
 * NPPVpluginScriptableIID
 */

NPError
NPP_GetValue (NPP instance, NPPVariable variable, void *value)
{
	PluginInstance *pi;
	NPError err = NPERR_NO_ERROR;

	switch (variable) {
		case NPPVpluginNameString:
			*((char **) value) = PLUGIN_NAME;
			break;
		case NPPVpluginDescriptionString:
			*((char **) value) = PLUGIN_DESCRIPTION;
			break;
		case NPPVpluginScriptableNPObject:
			pi = (PluginInstance*) instance->pdata;
			*(NPObject **) value = mep_getscriptableobject (pi);
			break;
		default:
			err = NPERR_GENERIC_ERROR;
	}
	return err;
}

int16
NPP_HandleEvent (NPP instance, void *event)
{
	/*
	 * needed for windowless mode
	 */
	XEvent *ev = (XEvent *) event;
	printf ("event occured\n");
	return TRUE;
}

NPError
NPP_Initialize (void)
{
	ecore_init ();
	ecore_config_init ("mozilla_edje_plugin");
	evas_init ();
	edje_init ();

  return NPERR_NO_ERROR;
}

#ifdef OJI
jref
NPP_GetJavaClass ()
{
	return NULL;
}
#endif

void
NPP_Shutdown (void)
{
	edje_shutdown ();
	evas_shutdown ();
	ecore_config_shutdown ();
	ecore_shutdown ();
}

NPError 
NPP_New (NPMIMEType pluginType,
    NPP instance,
    uint16 mode,
    int16 argc,
    char* argn[],
    char* argv[],
    NPSavedData* saved)
{
	if (instance == NULL)
		return NPERR_INVALID_INSTANCE_ERROR;
	instance->pdata = NPN_MemAlloc (sizeof (PluginInstance));
	
	PluginInstance *pi = (PluginInstance*) instance->pdata;
	if (pi == NULL) 
		return NPERR_OUT_OF_MEMORY_ERROR;
	memset(pi, 0, sizeof (PluginInstance));

	/* mode is NP_EMBED, NP_FULL, or NP_BACKGROUND (see npapi.h) */
	pi->mode = mode;
	pi->type = strdup ("application/edje");
	pi->instance = instance;

	pi->pluginsPageUrl = NULL;
	pi->exists = FALSE;

	/* Parse argument list passed to plugin instance */
	/* We are interested in these arguments
	 *  PLUGINSPAGE = <url>
	 */
	while (argc > 0)
	{
		argc --;
		if (argv[argc] != NULL)
		{
			if (!PL_strcasecmp (argn[argc], "PLUGINSPAGE"))
				pi->pluginsPageUrl = strdup (argv[argc]);
			else if (!PL_strcasecmp (argn[argc], "PLUGINURL"))
				pi->pluginsFileUrl = strdup (argv[argc]);
			else if (!PL_strcasecmp (argn[argc], "CODEBASE"))
				pi->pluginsPageUrl = strdup (argv[argc]);
			else if (!PL_strcasecmp (argn[argc], "CLASSID"))
				pi->pluginsFileUrl = strdup (argv[argc]);
			else if (!PL_strcasecmp (argn[argc], "HIDDEN"))
				pi->pluginsHidden = (!PL_strcasecmp (argv[argc], "TRUE"));
			else if (!PL_strcasecmp (argn[argc], "DATA"))
				pi->data = strdup (argv[argc]);
			else if (!PL_strcasecmp (argn[argc], "GROUP"))
				pi->group = strdup (argv[argc]);
			else if (!PL_strcasecmp (argn[argc], "THEME"))
				pi->theme = strdup (argv[argc]);
		}
	}

	// create evas for this plugin instance
	pi->evas = evas_new ();

	// configure plugin instance
	mep_configure (pi);

	// set windowless
	/*
	if (NPN_SetValue (instance, NPPVpluginWindowBool, FALSE))
		printf ("could not set windowless mode\n");
	if (NPN_SetValue (instance, NPPVpluginTransparentBool, TRUE))
		printf ("could not set transparent mode\n");
	*/

	return NPERR_NO_ERROR;
}

NPError 
NPP_Destroy (NPP instance, NPSavedData** save)
{
	if (instance == NULL)
			return NPERR_INVALID_INSTANCE_ERROR;

	PluginInstance *pi = (PluginInstance*) instance->pdata;

	pi->done = 1;

	if (pi != NULL) {
		if (pi->type)
			NPN_MemFree(pi->type);
		if (pi->pluginsPageUrl)
			NPN_MemFree(pi->pluginsPageUrl);
		if (pi->pluginsFileUrl)
			NPN_MemFree(pi->pluginsFileUrl);
		if (pi->edj)
			evas_object_del (pi->edj);
		if (pi->skin)
			evas_object_del (pi->skin);
		if (pi->evas)
			evas_free (pi->evas);
		if (pi->data)
			NPN_MemFree (pi->data);
		if (pi->group)
			NPN_MemFree (pi->group);
		if (pi->theme)
			NPN_MemFree (pi->theme);
		if (pi->config_theme)
			NPN_MemFree (pi->config_theme);
		if (pi->config_engine)
			NPN_MemFree (pi->config_engine);
		NPN_MemFree(instance->pdata);
		instance->pdata = NULL;
	}

	return NPERR_NO_ERROR;
}

NPError 
NPP_SetWindow (NPP instance, NPWindow *window)
{
	if (instance == NULL)
		return NPERR_INVALID_INSTANCE_ERROR;

	PluginInstance *pi = (PluginInstance *) instance->pdata;
	if (pi == NULL)
		return NPERR_INVALID_INSTANCE_ERROR;

	NPSetWindowCallbackStruct *ws_info = (NPSetWindowCallbackStruct *) window->ws_info;

	if (pi->window == (Window) window->window) {
		/* The page with the plugin is being resized.
			 Save any UI information because the next time
			 around expect a SetWindow with a new window
			 id.
		*/
		/*
		fprintf(stderr, "plugin received window resize.\n");
		fprintf(stderr, "Window=(%i)\n", (int)window);
		fprintf(stderr, "W=(%i) H=(%i)\n",
				(int)window->width, (int)window->height);
		*/

		return NPERR_NO_ERROR;
	} else {
		pi->window = (Window) window->window;
		pi->x = window->x;
		pi->y = window->y;
		pi->width = window->width;
		pi->height = window->height;
		pi->display = ws_info->display;
		pi->visual = ws_info->visual;
		pi->depth = ws_info->depth;
		pi->colormap = ws_info->colormap;

		int output_method = evas_render_method_lookup (pi->config_engine);
		evas_output_method_set (pi->evas, output_method);
		evas_output_size_set (pi->evas, pi->width, pi->height);
		evas_output_viewport_set (pi->evas, 0, 0, pi->width, pi->height);
		if (output_method == evas_render_method_lookup ("software_x11"))
		{
			Evas_Engine_Info_Software_X11 *einfo = (Evas_Engine_Info_Software_X11 *) evas_engine_info_get (pi->evas);
			/* the following is specific to the engine */
			einfo->info.display = pi->display;
			einfo->info.visual = pi->visual;
			einfo->info.colormap = pi->colormap;
			einfo->info.drawable = (Drawable) pi->window;
			einfo->info.depth = pi->depth;
			einfo->info.rotation = 0;
			einfo->info.debug = 0;
			evas_engine_info_set(pi->evas, (Evas_Engine_Info *) einfo);
		}
		else if (output_method == evas_render_method_lookup ("gl_x11"))
		{
			Evas_Engine_Info_GL_X11 *einfo = (Evas_Engine_Info_GL_X11 *) evas_engine_info_get (pi->evas);
			/* the following is specific to the engine */
			einfo->info.display = pi->display;
			einfo->info.visual = pi->visual;
			einfo->info.colormap = pi->colormap;
			einfo->info.drawable = pi->window;
			einfo->info.depth = pi->depth;
			evas_engine_info_set(pi->evas, (Evas_Engine_Info *) einfo);
		}
		else
			printf ("no supported engine found\n");

		// add animator to render evas
		ecore_animator_add (mep_animator, pi);
		ecore_animator_frametime_set (1.0 / (float) pi->config_fps);
		edje_frametime_set (1.0 / (float) pi->config_fps);

		// add theme skin if stated
		if (!strcmp (pi->theme, "none"))
		{
			// do nothing
		}
		else if (!strcmp (pi->theme, "user"))
		{
			pi->skin = edje_object_add (pi->evas);
			char PATH [256];
			sprintf (PATH, "%s/%s", THEMESDIR, pi->config_theme);
			edje_object_file_set (pi->skin, PATH, "mozilla_edje_plugin/theme");
			evas_object_resize (pi->skin, pi->width, pi->height);
			evas_object_show (pi->skin);
			evas_object_focus_set (pi->skin, 1);
		}
		else
		{
			pi->skin = edje_object_add (pi->evas);
			edje_object_file_set (pi->skin, pi->theme, "mozilla_edje_plugin/theme");
			evas_object_resize (pi->skin, pi->width, pi->height);
			evas_object_show (pi->skin);
			evas_object_focus_set (pi->skin, 1);
		}

		pi->edj = edje_object_add (pi->evas);
		edje_object_file_set (pi->edj, pi->data, pi->group);
		evas_object_data_set (pi->edj, "pi", (void *) pi);

		evas_object_resize (pi->edj, pi->width, pi->height);
		evas_object_show (pi->edj);
		evas_object_focus_set (pi->edj, 1);
		evas_event_feed_mouse_in (pi->evas, 0, NULL);

		if (pi->skin) // if not none
		{
			edje_object_part_swallow (pi->skin, "data", pi->edj);
			edje_object_signal_callback_add (pi->skin, "ui,play", "*", mep_cb_ui_play, pi);
			edje_object_signal_callback_add (pi->skin, "ui,pause", "*", mep_cb_ui_pause, pi);
			edje_object_signal_callback_add (pi->skin, "ui,config", "*", mep_cb_ui_config, pi);
			edje_object_signal_callback_add (pi->skin, "ui,save", "*", mep_cb_ui_save, pi);
		}
		edje_object_signal_callback_add (pi->edj, "*", "*", mep_cb_all, pi);

		// set up message system
		Widget xtwidget = XtWindowToWidget (pi->display, pi->window);
		long event_mask = NoEventMask |
											ExposureMask |
											ButtonPressMask |
											ButtonReleaseMask |
											PointerMotionMask |
											StructureNotifyMask |
											SubstructureNotifyMask |
											EnterWindowMask |
											LeaveWindowMask;

		XSelectInput (pi->display, pi->window, event_mask);
		XtAddEventHandler (xtwidget, event_mask, True, mep_event_handler, pi);
		pi->atom = XInternAtom (pi->display, "edje_atom", False);

		pi->msg.type = ClientMessage;
		pi->msg.display = pi->display;
		pi->msg.window = pi->window;
		pi->msg.message_type = pi->atom;
		pi->msg.format = 32;
		pi->msg.data.l[0] = 0;

		pi->done = 0;

		// initialize asynccallback loop
		mep_asynccall (pi);
	}

	return NPERR_NO_ERROR;
}

NPError 
NPP_NewStream (NPP instance,
          NPMIMEType type,
          NPStream *stream, 
          NPBool seekable,
          uint16 *stype)
{
	if (instance == NULL)
		return NPERR_INVALID_INSTANCE_ERROR;

	return NPERR_NO_ERROR;
}

int32 
NPP_WriteReady (NPP instance, NPStream *stream)
{
	if (instance == NULL)
		return NPERR_INVALID_INSTANCE_ERROR;

	/* We don't want any data, kill the stream */
	NPN_DestroyStream(instance, stream, NPRES_DONE);

	/* Number of bytes ready to accept in NPP_Write() */
	return -1L;   /* don't accept any bytes in NPP_Write() */
}

int32 
NPP_Write (NPP instance, NPStream *stream, int32 offset, int32 len, void *buffer)
{
	if (instance == NULL)
		return NPERR_INVALID_INSTANCE_ERROR;

	/* We don't want any data, kill the stream */
	NPN_DestroyStream(instance, stream, NPRES_DONE);

	return -1L;   /* don't accept any bytes in NPP_Write() */
}

NPError 
NPP_DestroyStream (NPP instance, NPStream *stream, NPError reason)
{
	if (instance == NULL)
		return NPERR_INVALID_INSTANCE_ERROR;

	/***** Insert NPP_DestroyStream code here *****\
	PluginInstance* pi;
	pi = (PluginInstance*) instance->pdata;
	\**********************************************/

	return NPERR_NO_ERROR;
}

void 
NPP_StreamAsFile (NPP instance, NPStream *stream, const char* fname)
{
	/***** Insert NPP_StreamAsFile code here *****\
	PluginInstance* pi;
	if (instance != NULL)
			pi = (PluginInstance*) instance->pdata;
	\*********************************************/
}

void
NPP_URLNotify (NPP instance, const char* url,
                NPReason reason, void* notifyData)
{
	/***** Insert NPP_URLNotify code here *****\
	PluginInstance* pi;
	if (instance != NULL)
			pi = (PluginInstance*) instance->pdata;
	\*********************************************/
}

void 
NPP_Print (NPP instance, NPPrint* printInfo)
{
	if(printInfo == NULL)
		return;

	if (instance != NULL) {
	/***** Insert NPP_Print code here *****\
			PluginInstance* pi = (PluginInstance*) instance->pdata;
	\**************************************/
	
		if (printInfo->mode == NP_FULL) {
				/*
				 * PLUGIN DEVELOPERS:
				 *  If your plugin would like to take over
				 *  printing completely when it is in full-screen mode,
				 *  set printInfo->pluginPrinted to TRUE and print your
				 *  plugin as you see fit.  If your plugin wants Netscape
				 *  to handle printing in this case, set
				 *  printInfo->pluginPrinted to FALSE (the default) and
				 *  do nothing.  If you do want to handle printing
				 *  yourself, printOne is true if the print button
				 *  (as opposed to the print menu) was clicked.
				 *  On the Macintosh, platformPrint is a THPrint; on
				 *  Windows, platformPrint is a structure
				 *  (defined in npapi.h) containing the printer name, port,
				 *  etc.
				 */

/***** Insert NPP_Print code here *****\
				void* platformPrint =
						printInfo->print.fullPrint.platformPrint;
				NPBool printOne =
						printInfo->print.fullPrint.printOne;
\**************************************/
				
				/* Do the default*/
				printInfo->print.fullPrint.pluginPrinted = FALSE;
		}
		else {  /* If not fullscreen, we must be embedded */
				/*
				 * PLUGIN DEVELOPERS:
				 *  If your plugin is embedded, or is full-screen
				 *  but you returned false in pluginPrinted above, NPP_Print
				 *  will be called with mode == NP_EMBED.  The NPWindow
				 *  in the printInfo gives the location and dimensions of
				 *  the embedded plugin on the printed page.  On the
				 *  Macintosh, platformPrint is the printer port; on
				 *  Windows, platformPrint is the handle to the printing
				 *  device context.
				 */

/***** Insert NPP_Print code here *****\
				NPWindow* printWindow =
						&(printInfo->print.embedPrint.window);
				void* platformPrint =
						printInfo->print.embedPrint.platformPrint;
\**************************************/
		}
	}
}

