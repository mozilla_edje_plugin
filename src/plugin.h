#define TARGET              "_blank"
#define MIME_TYPES_HANDLED  "application/edje:Enlightenment Foundation Libraries (EFL) Edje theme file"
#define PLUGIN_NAME         "Mozilla Edje Plugin"
#define PLUGIN_DESCRIPTION  "This Plugin is able to preview Edje files through the evas X11 engines."

#include <stdio.h>
#include <string.h>
#include <pthread.h>

#include <npapi.h>
#include <npupp.h>
#include <npruntime.h>

#include <X11/Intrinsic.h>
#include <X11/Xlib.h>

#include <Ecore.h>
#include <Ecore_Config.h>
#include <Evas.h>
#include <Evas_Engine_Software_X11.h>
#include <Evas_Engine_GL_X11.h>
#include <Edje.h>

typedef struct _PluginInstance
{
    uint16 mode;
    Window window;
    Display *display;
    uint32 x, y;
    uint32 width, height;
    NPMIMEType type;
    char *message;

    NPP instance;
    char *pluginsPageUrl;
    char *pluginsFileUrl;
    NPBool pluginsHidden;
    Visual* visual;
    Colormap colormap;
    unsigned int depth;

		Atom atom;
		Evas *evas;
		char *data;
		char *group;
		char *theme;
		Evas_Object *edj;
		Evas_Object *skin;
		XClientMessageEvent msg;
		pthread_t trigger_id;
		int done;

		// config
		char *config_theme;
		char *config_engine;
		int config_fps;

		// scriptability
		NPObject *scriptable_object;

    NPBool exists;  /* Does the widget already exist? */
    int action;     /* What action should we take? (GET or REFRESH) */

} PluginInstance;

void mep_asynccall (void *userData);
void mep_configure (PluginInstance *pi);
int mep_animator (void *data);
void *mep_trigger (void *data);
NPObject *mep_getscriptableobject (PluginInstance *pi);

void mep_cb_ui_play (void *data, Evas_Object *edj, const char *emission, const char *source);
void mep_cb_ui_pause (void *data, Evas_Object *edj, const char *emission, const char *source);
void mep_cb_ui_save (void *data, Evas_Object *edj, const char *emission, const char *source);
void mep_cb_ui_config (void *data, Evas_Object *edj, const char *emission, const char *source);
void mep_cb_all (void *data, Evas_Object *edj, const char *emission, const char *source);
void mep_event_handler (Widget xtwidget, void *data, XEvent *xevent, Boolean *b);

/*
 * scriptability
 */

typedef struct _mep_edje_NPObject mep_edje_NPObject;
typedef struct _mep_part_NPObject mep_part_NPObject;

struct _mep_edje_NPObject {
	NPClass *_class;
	uint32_t referenceCount;
	NPP instance;
};

struct _mep_part_NPObject {
	NPClass *_class;
	uint32_t referenceCount;
	NPP instance;
	char *part;
};

extern struct NPClass mep_part_class;
extern struct NPClass mep_edje_class;

